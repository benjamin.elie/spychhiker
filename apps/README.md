# Applications using the SpeechHiker library

## Presentation

This folder contains applications for voice transformation using SpeechHiker.

## Sparse conversion

The script `sparseconversion.py` can be used to perform voice conversion based on sparse decomposition of speech. It requires two phonetic dictionaries that contain typical phonetic realizations of each phonemes learned from a source dataset and a target dataset. The script is launched by the command :

```
python3 path_to_apps/sparseconversion.py input.wav -d=dir_to_save_output -o=output.wav -dict path_to_source_dictionary path_to_target_dictionary -nj=num_parallel_jobs -tag=tag_for_new_corpus_list

```

You can perform the conversion on a list of utterance. In that case, it should be in a text file with .rlist extension woth the following format :
```
spk_id utt_id path_to_audio_file channel start_time end_time
```

You will find an example of source and target dictionaries in the spychhiker/library folder that corresponds to normal speech (source) and speech with an oxygen mask (target). Thus, if you want to convert the speech sample in the demos, just type the following command (e.g. at the root of the spychhiker directory):

```
python3 apps/sparseconversion.py demos/initial.wav -o=../output.wav -dict library/source_dict.h5 library/mask_dict.h5

```

In this example, the dictionaries are learned on speech sampled at 8000 Hz, hence a output sampling rate of 8000 Hz.

Note that for the conversion of a single audio file, the options -d, -nj, and -tag are useless. 

## Articulatory perturbations

The script `artperturbation.py` can be used to perform voice conversion based on articulatory perturbation. It requires either fixed modification factors or a distribution file that contains the probability density function from which the modification factors will be randomly generated. The script is launched by the command :

```
python3 path_to_apps/artperturbation.py input.wav -d=dir_to_save_output -o=output.wav -f alpha_1 alpha_2 alpha_3 alpha_4 -t beta_1 beta_2 beta_3 beta_4 -dist=path_to_dist_file -nj=num_parallel_jobs -tag=tag_for_new_corpus_list -sro=output_sampling_rate -nw=len_win -nf=filter_order

```

where
* alpha_i is the perturbation factor applied to the formant trajectory i (default is 0 => no perturbation)
* if only one alpha is given, it is applied for all formant trajectories
* beta_i is the translation factor applied to the formant trajectory i (default is 1 => no translation)
* if only one beta is given, it is applied for all formant trajectories
* if a distribution file is given, the alpha and beta are not applied
* nf is the order of the smoothing filter applied to the formant trajectory estimation (default is 0 => no filter applied)
* the default sampling rate sro is the same as the original signal
* the default window length is 0.03 seconds


You can perform the conversion on a list of utterance. In that case, it should be in a text file with .rlist extension woth the following format :
```
spk_id utt_id path_to_audio_file channel start_time end_time
```

Note that for the conversion of a single audio file, the options -d, -nj, and -tag are useless.

### Examples:

Here are some examples that use the speech sample in the demos directory. Type the followng commands (e.g. at the root of the spychhiker directory):

#### Lower all formant frequencies by a common factor (e.g. 25%)

```
python3 apps/artperturbation.py demos/initial.wav -o=../output.wav -t 0.75

```

#### Lower the third formant frequency by factor (e.g. 30%)

```
python3 apps/artperturbation.py demos/initial.wav -o=../output.wav -t 1 1 0.7 1

```

## prosodic perturbations

The script `prosperturbation.py` can be used to perform voice conversion based on prosodic perturbation. Similarly to the articulatory perturbation, it requires either fixed modification factors or a distribution file that contains the probability density function from which the modification factors will be randomly generated. The script is launched by the command :

```
python3 path_to_apps/prosperturbation.py input.wav -d=dir_to_save_output -o=output.wav -f=span_modification_factor -t=translation_factor -dur=duration_factor -dist=path_to_dist_file -nj=num_parallel_jobs -tag=tag_for_new_corpus_list -sro=output_sampling_rate
```

where
* span_modification_factor is the modification factor applied to the span of the pitch contour i (default is 1 => no span modification)
* translation_factor is the translation factor applied to the pitch contour (default is 1 => no translation)
* duration_factor is the modification factor applied to the utterance duration (default is 1 => no modification)
* if a distribution file is given, the span and translation factors are not applied
* the default sampling rate sro is the same as the original signal


You can perform the conversion on a list of utterance. In that case, it should be in a text file with .rlist extension woth the following format :
```
spk_id utt_id path_to_audio_file channel start_time end_time
```

Note that for the conversion of a single audio file, the options -d, -nj, and -tag are useless.

### Examples:

Here are some examples that use the speech sample in the demos directory. Type the followng commands (e.g. at the root of the spychhiker directory):

#### Raise the pitch by 50%

```
python3 apps/prosperturbation.py demos/initial.wav -o=../output.wav -t=1.5

```

#### Flatten the pitch contour (no pitch variations)

```
python3 apps/prosperturbation.py demos/initial.wav -o=../output.wav -f=0 

```

#### Make the speaker speak faster by 20% 

```
python3 apps/prosperturbation.py demos/initial.wav -o=../output.wav -dur=0.8

```

#### Combine all effects

```
python3 apps/prosperturbation.py demos/initial.wav -o=../output.wav -t=1.5 -f=0 -dur=0.8

```


