#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 20 15:50:19 2021

@author: benjamin
"""

import spychhiker as sph
import argparse
import os
import multiprocessing
from joblib import Parallel, delayed
import numpy as np
from tqdm import tqdm

def sparse_augment_list(k_l, rlist, W_source, W_target, output_dir, 
                                                             tag=""):
   
    audio = rlist["path_to_utterance"][k_l]    
    path, file = os.path.split(audio)
    start_tmp = rlist["start"][k_l]
    end_tmp = rlist["end"][k_l]
    spk_id = rlist["speaker_id"][k_l]
    utt_id = rlist["utterance_id"][k_l]
    chn = rlist["channel"][k_l]
    
    _, file_ext = os.path.splitext(file)
    out_file_tmp = file.replace(file_ext, "_" + chn + "_" + start_tmp + "_" + 
                            end_tmp.replace("\n", "") + file_ext)
    out_file = os.path.join(output_dir, out_file_tmp) 
    new_utt_id = utt_id + "_" + start_tmp + "_" + end_tmp.replace("\n", "") + tag
    
    Sp = sph.file2speech(audio, sro=8000, chn=int(chn), start=float(start_tmp), 
                         end=float(end_tmp))
    new_end = sph.transform_sparse_audio(out_file_tmp, W_source, W_target,
                                             output_audio=out_file)
    new_end_string = "%.3f" %new_end + "\n"
   
    rlist_line = (spk_id + tag + " " + new_utt_id + " " + out_file +
                               " 1 0 " + new_end_string)

    return rlist_line

parser = argparse.ArgumentParser(description="Voice conversion based on sparse decomposition of speech.")
parser.add_argument("input", metavar="input", type=str,
                    help="path to the input audio or file list")
parser.add_argument("-d", dest="outdir", metavar="OutputDir", type=str,
                    help="directory where to save output data", default="tmp")
parser.add_argument("-nj", dest="njobs", metavar="njobs", type=int,
                    help="Number of jobs", default=16)
parser.add_argument("-o", dest="outname", metavar="output corpus name", type=str,
                    help="Name of the output audio file or file list", default="trn")
parser.add_argument("-dict", dest="dict_file", type=str, nargs="+",
                    metavar="path to the phonetic dictionaries",
                    help="file for feature distribution")
parser.add_argument("-tag", dest="tag", type=str, metavar="tag",
                    help="tag for new speaker labels", default=None)
parser.add_argument('-sro', dest='sro', metavar='sro', type=int,
                    help='Output sampling frequency', default=0)

corpus_name = parser.parse_args().input
output_dir = parser.parse_args().outdir
num_jobs = min(16, int(parser.parse_args().njobs))
output_name = parser.parse_args().outname
dict_file = parser.parse_args().dict_file
tag = parser.parse_args().tag
sro = parser.parse_args().sro
if sro <= 0: 
    sro = None

_, corpus_ext = os.path.splitext(corpus_name)

if corpus_ext == ".rlist":
    isWav = False
elif corpus_ext in [".wav", ".flac", ".mp3", ".ogg", ".sph"]:
    isWav = True
else:
    raise ValueError("Bad file type for the input")

if tag is None:
    tag = ""
else:
    tag = "_" + tag

if dict_file is not None:
    if len(dict_file) < 2:
        raise ValueError("2 dictionaries are required!")
    if len(dict_file) > 2:
        dict_file = dict_file[:1] 
    if not os.path.isfile(dict_file[0]):
        raise ValueError("First dictionary (" + dict_file[0] + ") does not exist")
    if not os.path.isfile(dict_file[1]):
        raise ValueError("Second dictionary (" + dict_file[1] + ") does not exist")
    A_input, ph_input, info_input = sph.read_dictionary_file(dict_file[0])
    A_output, ph_output, info_output = sph.read_dictionary_file(dict_file[1])    

    if info_input != info_output:
        raise ValueError("Dictionaries are not built with the same method")
    if list(ph_input) != list(ph_output):    
        A_input, A_output, phones = sph.merge_dictionary(A_input, A_output, 
                                                ph_input, ph_output)
    else:
        phones = ph_input
        
    if info_input in [b"ceps_zscore", b"ceps_log", b"ceps_coeff"]:
        A_input = (A_input - np.mean(A_input, 0, keepdims=True)) / np.std(A_input, 0, keepdims=True)
        A_output = (A_output - np.mean(A_output, 0, keepdims=True)) / np.std(A_output, 0, keepdims=True)
    elif info_input == b"ceps_var":
        A_input = A_input / np.std(A_input, 0, keepdims=True)
        A_output = A_output / np.std(A_output, 0, keepdims=True)

    
else:
    raise ValueError("No dictionary file given!")

if isWav:
    Sp = sph.file2speech(corpus_name, sro=8000)
    sph.transform_sparse_audio(Sp, A_input, A_output, 
                                     output_audio=output_name)

else:
    out_corpus_rlist = os.path.join(output_name + ".rlist")
    rlist = sph.parse_rlist_file(corpus_name)

    if os.path.isfile(out_corpus_rlist):
        os.remove(out_corpus_rlist)

    n_files_2_process = len(rlist["path_to_utterance"])
    rlistID = open(out_corpus_rlist, "w", encoding="Latin 1")
    output_dir_tmp = output_dir

    inputs = range(len(rlist["path_to_utterance"]))
    nn_jobs = min(num_jobs, len(inputs))

    print("Expected number of jobs is " + str(nn_jobs))
    print(str(len(rlist["path_to_utterance"])) + " files to process")

    pList = Parallel(n_jobs=nn_jobs, prefer="processes")(delayed(sparse_augment_list)(k, 
                          rlist, A_input, A_output, output_dir_tmp, tag) for k in tqdm(inputs))
    
    rlistID.writelines(pList)    
    rlistID.close()

