#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 20 15:50:19 2021

@author: benjamin
"""

import spychhiker as sph
import argparse
import os
import multiprocessing
from joblib import Parallel, delayed
import numpy as np
from tqdm import tqdm

def perturb_augment_list(k_l, rlist, pertubation_factor, translation_factor,
                         dur_factor, dist, output_dir, sro, tag=""):
   
    audio = rlist["path_to_utterance"][k_l]    
    path, file = os.path.split(audio)
    start_tmp = rlist["start"][k_l]
    end_tmp = rlist["end"][k_l]
    spk_id = rlist["speaker_id"][k_l]
    utt_id = rlist["utterance_id"][k_l]
    chn = rlist["channel"][k_l]
    
    
    
    _, file_ext = os.path.splitext(file)
    out_file_tmp = file.replace(file_ext, "_" + chn + "_" + start_tmp + "_" + 
                            end_tmp.replace("\n", "") + file_ext)
    out_file = os.path.join(output_dir, out_file_tmp) 
    new_utt_id = utt_id + "_" + start_tmp + "_" + end_tmp.replace("\n", "") + tag
    
    Sp = sph.file2speech(audio, sro=sro, chn=int(chn), start=float(start_tmp), 
                         end=float(end_tmp))
    Sp.change_prosody(dil_fact=perturbation_factor, trans_fact=translation_factor,
                         dur_fact=dur_factor, dist=dist)
    Sp.savespeech(out_file, "normalized")
    
    new_end_string = "%.3f" %(len(Sp.signal) / Sp.sampling_frequency) + "\n"
   
    rlist_line = (spk_id + tag + " " + new_utt_id + " " + out_file +
                               " 1 0 " + new_end_string)

    return rlist_line

parser = argparse.ArgumentParser(description="Voice transformation based on prosodic perturbations")
parser.add_argument("input", metavar="input", type=str,
                    help="path to the input audio or file list")
parser.add_argument("-d", dest="outdir", metavar="OutputDir", type=str,
                    help="directory where to save output data", default="tmp")
parser.add_argument("-nj", dest="njobs", metavar="njobs", type=int,
                    help="Number of jobs", default=16)
parser.add_argument("-o", dest="outname", metavar="output corpus name", type=str,
                    help="Name of the output audio file or file list", 
                    default="trn")
parser.add_argument('-f', '--factor', dest='pert_factor', type=float,
                    metavar='pitch perturbation factor',
                    help='pitch perturbation factor (default = 1, no modification)', 
                    default=1)
parser.add_argument('-t', '--trans', dest='translation_factor', type=float,
                    metavar='pitch translation factor', 
                    help='pitch translation factor (default = 1, no translation)', 
                    default=1)
parser.add_argument('-dur', '--dur', dest='duration_factor', type=float,
                    metavar='duration_factor', 
                    help='duration modification factor (default = 1, no modification)', 
                    default=1)
parser.add_argument('-sro', dest='sro', metavar='sro', type=int,
                    help='Output sampling frequency', default=0)
parser.add_argument('-dist', dest='dist_file', type=str,
                    metavar='file for feature distribution',
                    help='file for feature distribution', default=None)
parser.add_argument('-tag', dest='tag', type=str, metavar='tag',
                    help='tag for lnew speaker labels', default=None)

corpus_name = parser.parse_args().input
output_dir = parser.parse_args().outdir
num_jobs = min(16, int(parser.parse_args().njobs))
output_name = parser.parse_args().outname
perturbation_factor = parser.parse_args().pert_factor
translation_factor = parser.parse_args().translation_factor
duration_factor = parser.parse_args().duration_factor
sro = parser.parse_args().sro
feature_file = parser.parse_args().dist_file
tag = parser.parse_args().tag

if tag is None:
    tag = ''
else:
    tag = '_' + tag

if feature_file is not None:    
    dist = sph.get_distribution_from_file(feature_file)
else:
    dist = None

_, corpus_ext = os.path.splitext(corpus_name)

if corpus_ext == ".rlist":
    isWav = False
elif corpus_ext in [".wav", ".flac", ".mp3", ".ogg", ".sph"]:
    isWav = True
else:
    raise ValueError("Bad file type for the input")

if sro <= 0: 
    sro = None

if isWav:
    Sp = sph.file2speech(corpus_name, sro=sro)
    Sp.change_prosody(dil_fact=perturbation_factor, 
                         trans_fact=translation_factor,
                         dur_fact=duration_factor, dist=dist)
    Sp.savespeech(output_name, "normalized")

else:
    out_corpus_rlist = os.path.join(output_name + ".rlist")
    rlist = sph.parse_rlist_file(corpus_name)

    if os.path.isfile(out_corpus_rlist):
        os.remove(out_corpus_rlist)

    n_files_2_process = len(rlist["path_to_utterance"])    
    output_dir_tmp = output_dir

    inputs = range(len(rlist["path_to_utterance"]))
    nn_jobs = min(num_jobs, len(inputs))

    print("Expected number of jobs is " + str(nn_jobs))
    print(str(len(rlist["path_to_utterance"])) + " files to process")

    pList = Parallel(n_jobs=nn_jobs, prefer="processes")(delayed(perturb_augment_list)(k, 
                          rlist, perturbation_factor, translation_factor, duration_factor, 
                          dist, output_dir_tmp, sro, tag) for k in tqdm(inputs))
    
    rlistID = open(out_corpus_rlist, "w", encoding="Latin 1")
    rlistID.writelines(pList)    
    rlistID.close()

