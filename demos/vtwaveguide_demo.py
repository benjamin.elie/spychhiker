#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 14:37:38 2020

@author: benjamin
"""
import spychhiker as sph
import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

# Import area function. The example is a /a/, in the library folder
file2import = '../../SpeechHiker/library/a_example.json';
af, lf, wvg = sph.importjson(file2import)

# plot area function
wvg.area_function.plot()

# compute the transfer function
# First, we need to define the physical parameters. You can load them in
# the library...
Const = sph.timestruct()
Const.freq = np.arange(1, 5001, 10)
transFun, freq = wvg.computetransferfunction(Const)

# plot the transfer function
h_TF = wvg.plottransferfunction()

# # Compute formants
formFreq = wvg.computeformants()

# plot formants
for k in range(len(formFreq)):
    idx = np.argmin(abs(freq - formFreq[k]))
    plt.plot(formFreq[k], abs(transFun[idx]), marker='o', markerfacecolor='r', 
             markeredgecolor='k', markersize=10)
tt = plt.title('Transfer function of /a/')
# leg = plt.legend('Transfer function', 'Formants')
