#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 14:37:38 2020

@author: benjamin
"""
import spychhiker as sph
import matplotlib.pyplot as plt

plt.close('all')

# Import and read an audio file
wavFile = 'input_data/azha.wav'
Sp = sph.file2speech(wavFile)

# Import and read segmentation file
segFile = 'input_data/azha.TextGrid'
Sp.alignread(segFile)

### Get the pitch
Sp.getpitch()
# Plot on spectrogram
Sp.computespectrogram(512) # First compute the narrow-band spectrogram
Sp.spectrogram.tfplot(pitch_plot=True) # Then plot it with the pitch contour

# Get formants
Sp.formantestimate()
tForm = Sp.formants.time_vector
formants = Sp.formants.frequency
# Plot on spectrogram
Sp.computespectrogram(64) # First compute the wide-band spectrogram
Sp.spectrogram.tfplot(formant_plot=True) # Then plot it with formants