#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  6 16:50:26 2020

@author: benjamin
"""

import spychhiker as sph
import numpy as np
import matplotlib.pyplot as plt

plt.close('all')
json_file = './JSON_files_examples/asha_00010.json'
af, lf, VT = sph.importjson(json_file)
area_function = VT.area_function
h1 = area_function.plot()

area_function.changejawopening(0.5*np.pi/180, 25)
area_function.plot(hf=h1)

json_file = './JSON_files_examples/asha_00010.json'
af, lf, VT = sph.importjson(json_file)
area_function_2 = VT.area_function
h2 = area_function_2.plot()

area_function_2.changeprotrusion(0.2, 38)
area_function_2.plot(hf=h2)

area_function_2.areamorph(area_function, 10)
area_function_2.plot(hf=h1)

plt.show()