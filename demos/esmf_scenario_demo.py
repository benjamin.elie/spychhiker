#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 15 09:42:26 2020

@author: elie
"""

import spychhiker as sph
import matplotlib.pyplot as plt
import matplotlib
# matplotlib.use("TkAgg")

prm = sph.miscstruct()

prm.psubmax = 500
prm.fsim = 2e4
sro = 1.2e4

Const = sph.timestruct()
Const.namp = 3e-6 # Sets the level of frication noise

fileJSON = 'JSON_files_examples/scenario.json'
print('Reading scenario file')
VTobj = sph.vtjsonread(fileJSON, 'JSON_files_examples', prm)

print('Scenario file read')
print('Start simulation')
VTobj.esmfsynthesis(Const)
VTobj.outputresample(sro, prm.fsim)
p_o = VTobj.pressure_radiated
MOT_in, Chink_in = VTobj.waveguide
Gl = VTobj.oscillators
# Parameters for the spectrogram
nwin = 64 # Wide-band spectrogram
novlp = nwin * 3 / 4 # 75% overlap
nfft = 2**sph.nextpow2(sro)

# We use SpeechAudio object
SP = sph.SpeechAudio('signal', p_o,
    'sampling_frequency', sro)
# Save in audio file
SP.savespeech("asha.wav", "normalized")
SP.phonetic_labels = [VTobj.phonetic_label]
SP.phonetic_instants = [VTobj.phonetic_instant]

# Plot input parameters
plt.close('all')
VTobj.waveguide = [MOT_in, Chink_in]
VTobj.oscillators = Gl
VTobj.plotinputparameters()

# Plot spectrogram
SP.computespectrogram(nwin=nwin, win='hanning', 
                      nfft=nfft, novlp=novlp)
h_spectro = SP.spectrogram.tfplot()

# Plot output parameters
VTobj.plotoutputparameters()
plt.show()
