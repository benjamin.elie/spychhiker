#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 23 12:52:59 2020

@author: benjamin
"""

import spychhiker as sph
import numpy as np
import matplotlib.pyplot as plt

## initialization
# Define the target formants, estimated from real /a/ (female speaker)
form_target = np.array([885.9, 1590, 2901, 4290])

# Import area function. 
# The example is a /a/ obtained from MRI of a male subject
# It will serve as the initial solution
file2import = '../library/a_example.json'
af, lf, wvg = sph.importjson(file2import)

# plot area function of the initial solution
plt.close('all')
af_fig = plt.figure()
ax1 = plt.subplot(221)
ax1.plot(np.cumsum(lf), af, '--k')
plt.xlabel('Distance from glottis (m)')
plt.ylabel('Area (cm\textsuperscript{2})')
plt.title('Area function')


# plot the transfer function of the initial solution
Const = sph.timestruct()
Const.freq = np.arange(1, 5000, 10)
Const.tf_method = 'cmp'
init_TF, freq = wvg.computetransferfunction(Const, Const.tf_method)
ax2 = plt.subplot(212)
ax2.plot(Const.freq, abs(init_TF),'--k')
plt.yscale('log')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Transfer function')
init_form = wvg.computeformants()

# plot formants
idx = []
for k in range(len(init_form)):
    idx.append(np.argmin(np.abs(freq-init_form[k])))  
ax2.plot(init_form, np.abs(init_TF[idx]), 'sk', markerfacecolor='k',
        markersize=10)

## Inversion
# set the parameters for the inversion
Const.threshold = 0.1
Const.potential_weight = 1e-4
Const.psi = 0.5
new_af, new_lf, fnest, Hnest, err_form = wvg.formant2af(form_target, Const)

## Plot results
# Compute formants
new_TF, dum = wvg.computetransferfunction(Const, Const.tf_method)
new_form = wvg.computeformants()

# plot new area function
ax1.plot(np.cumsum(new_lf), new_af, 'r', linewidth=2)

ax2.plot(Const.freq, abs(new_TF),'r', linewidth=2)
plt.yscale('log')
# plot formants
idx = []
for k in range(len(new_form)):
    idx.append(np.argmin(np.abs(freq-new_form[k])))  
ax2.plot(new_form, np.abs(new_TF[idx]), 'ok', markerfacecolor='r', 
        markersize=10)
idx = []
for k in range(len(form_target)):
    idx.append(np.argmin(np.abs(freq-form_target[k])))  
ax2.plot(form_target, abs(new_TF[idx]), 'vk', markerfacecolor='b',
        markersize=10)
plt.show()

