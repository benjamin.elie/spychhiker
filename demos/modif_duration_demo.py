#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 14:37:38 2020

@author: benjamin
"""
import spychhiker as sph
import matplotlib.pyplot as plt
import numpy as np

plt.close('all')

# Import and read an audio file
wavFile = 'input_data/azha.wav'
Sp = sph.file2speech(wavFile)

# Import and read segmentation file
segFile = 'input_data/azha.TextGrid'
Sp.alignread(segFile)
# Plot on spectrogram
Sp.computespectrogram(64) # First compute the narrow-band spectrogram
Sp.spectrogram.tfplot() # Then plot it 

### We want to modify the length of the fricative by a factor 2
# 1. Get the phonetic labels and instants
phon_label = Sp.phonetic_labels[0]
phon_instant = Sp.phonetic_instants[0]
# 2. Set the time points and factors (1 everywhere for the moment)
pts = phon_instant[:-1,1]
factor = np.ones(len(pts) + 1) 
# 3. Find the index where is the fricative (here Z)
idxFric = [ x for x in range(len(phon_label)) if phon_label[x] in ['Z'] ]
# 4. Set the factor vector to 2 at this index 
factor[idxFric] = 2
# 5. change durations
SpOut = Sp.copy()
SpOut.changeduration(pts, factor)
# 6. plot spectrogram
SpOut.computespectrogram(64) # First compute the narrow-band spectrogram
SpOut.spectrogram.tfplot() # Then plot it 
SpOut.savespeech('duration_long.wav', 'normalized')