#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 14:37:38 2020

@author: benjamin
"""
import spychhiker as sph
import matplotlib.pyplot as plt

plt.close('all')

# Import and read an audio file
wavFile = 'input_data/lours.wav'
Sp = sph.file2speech(wavFile)
# 1. Save the audio
Sp.savespeech('initial.wav', 'normalized')

#### Changing pitch :
# 1. Get the pitch
Sp.getpitch()
# 2. Plot Contour
Sp.pitch.plot()
# 3. Create a pitch object which will be the target. Copy from the original pitch
outPitch = Sp.pitch.copy()
# 4. Plot the copy to check whether they are similar
outPitch.plot()
# 5. Shift down by a factor 2
outPitch.shift(0.5)
# 6. Modify audio so that pitch matches the target
Sp.changecontour(outPitch)
# 7. Save the audio
Sp.savespeech('pitch_shift.wav', 'normalized')

## Now, we want to change the pitch span (or pitch range)
# 1. Repeat the first steps to create a new pitch object
Sp = sph.file2speech(wavFile)
initPitch = Sp.getpitch()
outPitch = Sp.pitch.copy()
# 2. multiply the pitch span by a factor 2
outPitch.changespan(2)
# 3. Modify audio so that pitch matches the target
Sp.changecontour(outPitch)
# 4. Save the audio
Sp.savespeech('pitch_span.wav', 'normalized')
# 5. Compare pitch contours
tf0 = initPitch.time_points
initF0 = initPitch.values
outF0 = outPitch.values

plt.figure()
plt.plot(tf0, initF0, 'b')
plt.plot(tf0, outF0, 'r')

## Now, we would like to emphasize the first pitch peak
# 1. Repeat the first steps to create a new pitch object
Sp = sph.file2speech(wavFile)
initPitch = Sp.getpitch()
# 2. Get the stylized contour
initPitch.getcontour()
# 3. Find the local maxima and get the time point of the first peak
initPitch.contour.getlocalmax()
tMax = initPitch.time_points[initPitch.contour.locs_max[0]]
# 4. Create the pitch target by copy
outPitch = initPitch.copy()
# 5. Change the pitch contour by a factor 2
outPitch.contour.changespline(tMax, 2)
# 6. Modify audio so that pitch matches the target
Sp.changecontour(outPitch)
# 7. Save the audio
Sp.savespeech('pitch_emph.wav', 'normalized')
# 8. Compare pitch contours
tf0 = initPitch.time_points
initF0 = initPitch.values
outF0 = outPitch.values

plt.figure()
plt.plot(tf0, initF0, 'b')
plt.plot(tf0, outF0, 'r')